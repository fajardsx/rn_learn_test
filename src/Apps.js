import React from 'react';

import { Provider, connect } from "react-redux";
import { createStackNavigator, createAppContainer } from 'react-navigation';

import { createStore, applyMiddleware, combineReducers } from 'redux'

import {
    createReactNavigationReduxMiddleware,
    createNavigationReducer,
    createReduxContainer
} from 'react-navigation-redux-helpers'

import { WelcomeVM, DetailVM } from './container'
import ReduxNavigation from './reduxnavigation'

const AppNavigation = createStackNavigator(
    {
        welcome: {
            screen: WelcomeVM
        },
        detail: {
            screen: DetailVM
        }
    }, {
        initialRouteName: 'welcome'
    }
)


const navReducer = createNavigationReducer(AppNavigation);

const middleware = createReactNavigationReduxMiddleware(
    state => state.nav,
    'root'
);

export const AppsIndex = createAppContainer(AppNavigation);

const store = createStore(
    combineReducers({
        nav: navReducer,
    }),
    applyMiddleware(middleware)
);

export default class Root extends React.Component {
    render() {
        return <Provider store={store}>
            <ReduxNavigation />
        </Provider>
    }
}

