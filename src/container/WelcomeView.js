import React from "react";
import { View, Text ,Button} from "react-native";


export default class WelcomeVM extends React.Component {
    static navigationOptions = {
        title: 'Welcome'
    }

    render() {
        return <View
            style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center'
            }}
        >
            <Text>Welcome</Text>
            <Button
                onPress={()=>this.props.navigation.navigate('detail')}
                title ="Go to Detail Screen"
            />
        </View>
    }
}