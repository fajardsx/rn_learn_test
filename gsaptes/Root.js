import React, { Component } from 'react'
import { Text, View ,TouchableOpacity} from 'react-native'
import posed from 'react-native-pose';
import Boxanim from "./boxanim";
import SwipeGesture from "../swipe-gesture";
const Box = posed.View({
    visible:{
        opacity:1,
        scaleY:1,
        transition:{
            opacity:{ease:'easeOut',duration:300},
            default:{ease:'linear',duration:500},
        }
    },
    hidden: {
        opacity: 1,
        scaleY: 0,
        transition:({toValue})=>({
            type:'keyframes',
            values:[0,10,toValue]
        })
    }
});
export default class Root extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
         width:0,
         isVisible:true
      }
    }
    onSwipePerformed = (action) => {
        switch (action) {
            case 'left': {
                console.log('left swipe performed');
                break;
            }
            case 'right': {
                console.log('right swipe performed')
                break;
            }
            default: {
                console.log('undetected action')
                break;
            }
        }
    }

    componentDidMount() {
       
    }
  render() {
      const {isVisible} = this.state;
    return (
      <View style={{ flex: 1 }}>
       
  
         <SwipeGesture 
            gestureStyle={{
                height:'100%',
                width:'100%',
                backgroundColor:'transparent'
            }}
            onSwipePerformed={this.onSwipePerformed}
         >
             <Text>this is swipe gesture</Text>
             <Text>Used to detect the user swipes and function accordingly</Text>
         </SwipeGesture>
            <TouchableOpacity
                style={{position: 'absolute'}}
                onPress={() => this.setState({ isVisible: !isVisible })}
            >
                <Box
                    style={{ width: 100, height: 100, backgroundColor: "#e00f32" }}
                    pose={this.state.isVisible ? "visible" : "hidden"}
                >
                    <Text>TEST</Text>
                </Box>

            </TouchableOpacity>
      </View>
    );
  }
}
