import React, { Component } from 'react';
import { View, Text,Animated ,StyleSheet} from 'react-native';
import SwipeGesture from '../swipe-gesture'

class Boxanim extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  onSwipePerformed = (action)=>{
      switch (action) {
          case 'left':{
              console.log('left swipe performed')
          }
          case 'right':{
              console.log('right swipe performed')
          }
         
      }
  }

  componentWillMount() {
    this.position = new Animated.ValueXY(0, 0);
    Animated.spring(this.position, {
      toValue: { x: 225, y: 225  }
    }).start();
  }

  render() {
    return (
      <Animated.View style={this.position.getLayout()}>
        <View style={styles.square} />
      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({
    square:{
        width:120,
        height:120,
        backgroundColor:'#00BCDA'
    }
});

export default Boxanim;
