import {AppsIndex} from '../'
import {NavigationActions} from 'react-navigation';


const initialAction = {type:NavigationActions.init}
//const initialState = AppsIndex.router.getStateForAction(initialAction)
const initialState={
    nameUser:'USER',
}

const UserReducer=(state=initialState,action)=>{
    switch (action.type) {
        case 'GET_NAME':
            return { ...state, nameUser: action.payload.nameUser}
        default:
            return {...state};
    }
}

export default UserReducer;