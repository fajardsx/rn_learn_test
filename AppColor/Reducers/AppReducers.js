import ColorReducer from './colorreducers'
import UserReducer from './userreducers';


export const reducerApp={
    color:ColorReducer,
    user:UserReducer
}