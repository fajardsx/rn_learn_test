import React from 'react';
import { createAppContainer, createStackNavigator } from 'react-navigation';
import { createStore, combineReducers, applyMiddleware} from 'redux';
import { Provider } from 'react-redux';
import {
    createReactNavigationReduxMiddleware,
    createNavigationReducer,
    createReduxContainer
} from 'react-navigation-redux-helpers'

import MainPage from './Components/mainpage';
import AppNavigator from './Components/appnavigator';
import ChooseColorPage from './Components/choosecolorpage';
import { reducerApp } from './Reducers/AppReducers';
import ProfilViewPage from './Components/profilpage';


const userSchema={
    name:'user_details',
    properties:{
        user_id:{type:'int',default:0},
        user_name:'string',
        user_contact:'string',
        user_address:'string'
    }
}
const AppIndexNavigation = createStackNavigator(
    {
       mainpage:{
           screen:MainPage
       },
       choosecolor:{
           screen:ChooseColorPage,
           navigationOptions:{
               headerLeft:null
           }
       },
       profilpageview:{
           screen: ProfilViewPage, navigationOptions: {
               headerLeft: null
           }
       }
    }, {
        initialRouteName: 'mainpage',
        mode:'modal'
    }
)

const navReducer = createNavigationReducer(AppIndexNavigation);

const navmiddleware = createReactNavigationReduxMiddleware(
    state=>state.nav
)

export const AppsIndex = createAppContainer(AppIndexNavigation)

const store= createStore(
    combineReducers({
        nav:navReducer,
        ...reducerApp
    }),
    applyMiddleware(navmiddleware)
)

export default class Root extends React.Component{
    constructor(props){
        super(props);
        //init realm
        
    }
    render(){
       return <Provider store={store}>
            {
                // AppNavigator place ColorIndex CreateStackNavigation
            }
            <AppNavigator/>
        </Provider>
    }
}