import React from 'react';
import { View,Button,Alert } from 'react-native';
import {connect} from 'react-redux';
import { COLORS } from '../State/colors';
import {userChanged} from '../Actions/colorchangeactions'

const USER = 'USER';

class MainPage extends React.Component{
    constructor(props){
        super(props)

    }
    onChooseColor(){
        this.props.navigation.navigate('choosecolor')
    }
    onChooseProfil(){
        this.props.userChanged('USER')
        this.props.navigation.navigate('profilpageview')
    }
    selectedColor(){
        const {colorName} = this.props;
        return COLORS[colorName].hexCode;
    }
    onChooseNewUser(){
        const name="fajar";
        const contact = "090292029";
        const address="jalan panjang"

       
        
    }
    render(){
        const color =  this.selectedColor();
        return <View style={{
            flex:1,
            alignSelf:'stretch',
            backgroundColor:color,
            justifyContent:'center',
            alignContent:'center'
        }}>
            <Button
                ///style={{backgroundColor:blue}}
                onPress={this.onChooseColor.bind(this)}
                color='#000'
                title="Choose Color"
            />
            <Button
                ///style={{backgroundColor:blue}}
                onPress={this.onChooseNewUser.bind(this)}
                color='#000'
                title="create new user"
            />
            <Button
                ///style={{backgroundColor:blue}}
                onPress={this.onChooseProfil.bind(this)}
                color='#000'
                title="Profil"
            />
        </View>
    }
}

const mapStateToProps= state =>{
    return {colorName:state.color.colorName}
}

export default connect(mapStateToProps,{userChanged})(MainPage);