import React,{Component  } from "react";
import { View,Button } from "react-native";
import {connect} from 'react-redux';
import {colorChanged} from '../Actions/colorchangeactions'
import {COLORS} from '../State/colors'

class ChooseColorPage extends Component{
    onSelectColor(colorName){
        console.log(colorName);
        this.props.colorChanged({ colorName})
        this.props.navigation.goBack()
    }

    render(){
        return <View style={{
            flex:1,
            backgroundColor:'#fff',
            justifyContent:'center',
            alignContent:'center'
        }}>
            {Object.keys(COLORS).map((key)=>{
                return <Button
                    key={key}
                    title={COLORS[key].name}
                    color={COLORS[key].hexCode}
                    onPress={()=>this.onSelectColor(key)}
                />
            })}
        </View>
    }
}
const mapStateToProps= state=>({})
export default connect(mapStateToProps,{colorChanged})(ChooseColorPage);