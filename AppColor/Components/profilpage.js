import React, { Component } from "react";
import { View, Text } from "react-native";
import { connect } from 'react-redux';

import {User} from '../State/colors';


class ProfilViewPage extends Component {
    constructor(props){
        super(props);
        //console.log(User);
        const { nameUser } = this.props;
        console.log(nameUser)
    }
    myname(){
        const { nameUser} = this.props;
        let getName = User[nameUser].value;
       
        return getName
    }
    render() {
       // const names = this.myname();

        return <View style={{
            flex: 1,
            backgroundColor: '#fff',
            justifyContent: 'center',
            alignContent: 'center'
        }}>
           <Text>{"my name : "}</Text>
        </View>
    }
}
const mapStateToProps = state => {
    return { nameUser: state.user.nameUser}
}
export default connect(mapStateToProps)(ProfilViewPage);