import React from 'react'
import {View,Text} from 'react-native'
import FilterLink from '../containers/FilterLink'
import {VisibilityFilters} from '../component_redux/actions'

const Footers=()=>{
    return <View >
        <Text>Show:</Text>
        <View style={{flexDirection:'row',justifyContent:'space-around'}}>
            <FilterLink filter={VisibilityFilters.SHOW_ALL} >ALL</FilterLink>
            <FilterLink filter={VisibilityFilters.SHOW_ACTIVE} >ACTIVE</FilterLink>
            <FilterLink filter={VisibilityFilters.SHOW_COMPLETE} >COMPLETED</FilterLink>
        </View>
       
    </View>
}

export default Footers;