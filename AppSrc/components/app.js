import React from 'react';
import {View,Text} from 'react-native'
import Footers from './footers';
import VisibilityTodoList from '../containers/VisibilityTodoList';
import AddTodo from '../containers/AddTodo';

console.disableYellowBox=true;
const App=()=>{
    return <View>
        <AddTodo/>
        <VisibilityTodoList/>
        <Footers/>
    </View>
}

export default App;