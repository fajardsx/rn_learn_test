import React from 'react'
import { Button } from 'react-native'
import PropTypes from 'prop-types'

const Link=({active,children,onclick})=>{

    return <Button
            onPress={onclick}
            disabled={active}
            style={{
                marginLeft:'5%'
            }}
            title={children}
    />
}

Link.propTypes={
    active:PropTypes.bool.isRequired,
    children:PropTypes.node.isRequired,
    onclick:PropTypes.func.isRequired
}

export default Link