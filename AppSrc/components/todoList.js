import React from 'react'
import { ScrollView, Text } from 'react-native'
import PropTypes from 'prop-types'
import Todo from './todo'


const TodoList = ({ todos, toggleTodo }) => {
    return <ScrollView>
        {todos!=null && todos.length>0 && todos.map(item=>{
             <Todo key={item.id} {...item} onclick={() => toggleTodo(item.id)}/>
        })}
    </ScrollView>
}

TodoList.propTypes = {
    todos: PropTypes.arrayOf(
        PropTypes.shape({
            id:PropTypes.number.isRequired,
            completed:PropTypes.bool.isRequired,
            text:PropTypes.string.isRequired
        }).isRequired
    ),
    toggleTodo:PropTypes.func.isRequired
}

export default TodoList