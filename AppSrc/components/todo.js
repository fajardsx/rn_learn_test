import React from 'react'
import {TouchableOpacity,Text} from 'react-native'
import PropTypes from 'prop-types'


const Todo = ({onclick,completed,text})=>{
    return <TouchableOpacity
            onPress={onclick}
        >
        <Text style={{
            textDecorationLine:completed?'line-through':'none'
        }}>{text}</Text>
    </TouchableOpacity>
}

Todo.propTypes={
    onclick:PropTypes.func.isRequired,
    completed:PropTypes.bool.isRequired,
    text:PropTypes.string.isRequired
}

export default Todo;