import React from 'react'
import {View,Text,TextInput,Button} from 'react-native'
import {connect} from 'react-redux';
import {addTodo} from '../component_redux/actions'


const AddToDo = ({dispatch})=>{
    let input = "";
  
    function onChanges(e) {
        console.log(e)
        input =e;
    }
    function onSubmit() {
        console.log(dispatch)
        //console.log(addTodo(input))
        dispatch(addTodo(input))
    }
    return <View>
            <TextInput
                ref={refs=>{inputs=refs}}
               onChangeText={txt=>{onChanges(txt)}}
            />
            <Button
            onPress={() => onSubmit()}
            title={'Submit'}
            />
    </View>
}

export default connect()(AddToDo)