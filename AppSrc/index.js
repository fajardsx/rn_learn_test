import React from 'React'

import {Provider} from 'react-redux'
import {createStore} from 'redux'

import  rootReducers  from './component_redux/reducers';
import App from './components/app';

const store = createStore(rootReducers)

export default class Apps extends React.Component{
    render(){
        return <Provider store={store}>
            <App/>
        </Provider>
    }
}